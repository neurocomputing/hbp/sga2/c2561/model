# Model documentation

## EDLUT BAXTER robot

Documentation for the EDLUT BAXTER robot is available here: https://github.com/EduardoRosLab/EDLUT_BAXTER

## MyoRobotic arm in the Neurorobotics Platform (NRP)

Three brain definitions have been implemented using common simulated/physical robots and interfaces. All these are provided below:

* A simulation model for the Neurorobotics Platform (NRP), available here: https://github.com/Roboy/musc-le_models
* A communication interface to the simulated and physical robot, available here: https://github.com/CARDSflow/cardsflow_gazebo/tree/kth
* The NRP experiment setup for the myorobotic arm, available here: https://github.com/Roboy/musc-le_experiment

### Spiking Cerebellum model

* The brain definition and control logic for the experiment, available here: https://gitlab.com/neurocomputing/myorobotic-arm/

### Feed-Forward network model

* The brain definition and control logic for the experiment, available here: https://github.com/HBPNeurorobotics/Demonstrator7-UnsupervisedHebbianLearning